#!/bin/bash

repo sync -c -j$(nproc --all) --force-sync --current-branch --no-clone-bundle --optimized-fetch --prune -q "device/xiaomi/land" "kernel/xiaomi/msm8937" "vendor/xiaomi" "vendor/evo-x_land"
