#!/bin/bash

source build/envsetup.sh
export LC_ALL=C
export ANDROID_QUIET_BUILD=true
export USE_CCACHE=1
export CCACHE_COMPRESS=1

lunch aosp_land-userdebug
